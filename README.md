# calamares

[![GitHub release](https://img.shields.io/github/release/calamares/calamares.svg)](https://github.com/calamares/calamares/releases) [![License](https://img.shields.io/badge/License-BSD%202--Clause-orange.svg)](https://opensource.org/licenses/BSD-2-Clause) [![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) [![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-lightgrey.svg)](http://creativecommons.org/publicdomain/zero/1.0/) [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![License: LGPL v2.0](https://img.shields.io/badge/License-LGPL%20v2.0-blue.svg)](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html) [![License: LGPL v2.1](https://img.shields.io/badge/License-LGPL%20v2.1-blue.svg)](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) [![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

<br><br>
How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/installers/calamares-installer/calamares.git
```
<br><br>

# Calamares installer without modifications

(Generic installer downloaded from the Calamares website)

Dependencies:

```
sudo pacman -S cmake extra-cmake-modules boost boost-libs qt5-svg qt5-webengine yaml-cpp networkmanager upower kcoreaddons kconfig ki18n kservice kwidgetsaddons kpmcore squashfs-tools rsync cryptsetup qt5-xmlpatterns doxygen dmidecode gptfdisk hwinfo kparts polkit-qt5 python qt5ct solid qt5-tools
```

PKGBUILD (with git):

```
# Maintainer: Rafael <rafael@rebornos.org>

pkgname=calamares
pkgver=3.2.39.3
pkgrel=1
pkgdesc="Distribution-Independent Installer Framework"
arch=('x86_64')
url="https://github.com/calamares/calamares"
license=('GPL')
makedepends=('git' 'cmake' 'extra-cmake-modules' 'kpmcore' 'boost-libs' 'yaml-cpp')
depends=('qt5-svg' 'qt5-webengine' 'yaml-cpp' 'networkmanager' 'boost' 'upower' 
         'kcoreaddons' 'kconfig' 'ki18n' 'kservice' 'kwidgetsaddons' 'kpmcore' 
         'squashfs-tools' 'rsync' 'cryptsetup' 'qt5-xmlpatterns' 'doxygen' 
         'dmidecode' 'gptfdisk' 'hwinfo' 'kparts' 'polkit-qt5' 'python' 'qt5ct' 
         'solid' 'qt5-tools')
provides=("${pkgname}")
source=("git+${url}.git")
sha512sums=('SKIP')

build() {
           mkdir -p ${pkgname}/build
           cd ${pkgname}/build
           cmake -DCMAKE_BUILD_TYPE=Debug ..
           make
}

package() {
           cd ${pkgname}/build
           make DESTDIR=${pkgdir} install
}
```

